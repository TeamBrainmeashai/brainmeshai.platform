﻿using BrainMesh.Platform.Components.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainMesh.Platform.Components.Interfaces
{
    public interface IChatContextManager
    {
        IdenifiedChatContext IdentifyContext(string sMessage, string ConversationId);
        bool IsMessageOfContext(string sMessage, string sContext);

    }
}
