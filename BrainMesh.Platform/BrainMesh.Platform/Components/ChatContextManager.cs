﻿using BrainMesh.Platform.Components.Interfaces;
using BrainMesh.Platform.Components.Objects;
using BrainMesh.Platform.Infrastructure.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BrainMesh.Platform.Components
{
    [Serializable]
    public class ChatContextManager : IChatContextManager
    {
        public const string CHATCONTEXTS = "CHATCONTEXTS";
        public const string CURRENTCHATCONTEXT = "CURRENTCHATCONTEXT";
        public const string NOTKNOWNCONTEXT = "NOTKNOWNCONTEXT";

        private ICacheManager _cache;
        private List<ChatContext> _knownContexts;

        public ChatContextManager(ICacheManager oCache)
        {
            _cache = oCache;
            object oContexts = _cache.ReadKey(CHATCONTEXTS);
            _knownContexts = JsonConvert.DeserializeObject<List<ChatContext>>(oContexts.ToString());
        }
        public IdenifiedChatContext IdentifyContext(string sMessage, string sConversationId)
        {
            IdenifiedChatContext oIdContext = new IdenifiedChatContext();

            List<ChatContext> oIdentifiedContexts = IdentifyContexts(sMessage);

            ChatContext oMostLikelyContext = oIdentifiedContexts.OrderByDescending(p => p.Priority).FirstOrDefault();

            if(oMostLikelyContext!= null)
            {
                oIdContext.Id = oMostLikelyContext.Id;
                oIdContext.Name = oMostLikelyContext.Name;
                oIdContext.Confidence = 0.9;
                _cache.WriteKey(sConversationId + "." + CURRENTCHATCONTEXT, oIdContext, DateTime.Now.AddHours(1));
                return oIdContext;
            }
            else
            {
                //Check for last known context
                object oContexts = _cache.ReadKey(sConversationId +"."+ CURRENTCHATCONTEXT);
                try
                {
                    ChatContext oContext = JsonConvert.DeserializeObject<ChatContext>(oContexts.ToString());
                    oIdContext.Id = oContext.Id;
                    oIdContext.Name = oContext.Name;
                    oIdContext.Confidence = 0.5;
                    return oIdContext;
                } catch (Exception e)
                { }
            }

            oIdContext.Id = Guid.Empty;
            oIdContext.Name = NOTKNOWNCONTEXT;
            oIdContext.Confidence = 0.9;
            return oIdContext;
        }

        public bool IsMessageOfContext(string sMessage, string sContext)
        {
            List<ChatContext> oIdentifiedContexts = IdentifyContexts(sMessage);

            return oIdentifiedContexts.Where(p => p.Name.ToLower() == sContext.ToLower()).Count() > 0;
        }

        private List<ChatContext> IdentifyContexts(string sMessage)
        {
            List<ChatContext> oIdentifiedContexts = new List<ChatContext>();

            foreach(ChatContext oContext in _knownContexts)
            {
                foreach(string sPattern in oContext.Signatures)
                {
                    Match result = Regex.Match(sMessage, sPattern);
                    
                    //Match found
                    if (result.Success)
                    {
                        if(!oIdentifiedContexts.Contains(oContext)) { oIdentifiedContexts.Add(oContext); }
                    }
                }
               
            }
            
            return oIdentifiedContexts;
        }
    }

    
}
