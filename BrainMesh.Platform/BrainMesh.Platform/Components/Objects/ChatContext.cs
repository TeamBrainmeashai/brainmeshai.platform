﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainMesh.Platform.Components.Objects
{
    public class ChatContext
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Priority { get; set; }
        public List<string> Signatures { get; set; }
    }
}
