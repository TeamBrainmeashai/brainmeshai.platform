﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainMesh.Platform.Components.Objects
{
    public class IdenifiedChatContext
    {
       public Guid Id { get; set; }
       public string Name { get; set; }
       public double Confidence { get; set; }
    }
}
