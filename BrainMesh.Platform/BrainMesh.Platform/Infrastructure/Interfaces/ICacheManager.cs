﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainMesh.Platform.Infrastructure.Interfaces
{
    public interface ICacheManager
    {
        bool PingServer();
        string ExecuteCommand(string sCommandName, string sParameters);
        object ReadKey(string sKeyName);
        bool WriteKey(string sKeyName, string sValue, DateTime? dtExpire);
        bool WriteKey(string sKeyName, object oValue, DateTime? dtExpire);
        void ClearCache();
    }
}
