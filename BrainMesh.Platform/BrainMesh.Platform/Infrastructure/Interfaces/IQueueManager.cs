﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BrainMesh.Platform.Infrastructure.Interfaces
{
    public interface IQueueManager
    {
        Task<bool> PushToQueue<T>(T Message, string sQueue);
    }
}
