﻿using BrainMesh.Platform.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Azure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Queue; // Namespace for Queue storage types
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace BrainMesh.Platform.Infrastructure
{
    [Serializable]
    public class CloudQueueManager : IQueueManager
    {
        string _conn = string.Empty;

        public CloudQueueManager(string sConectionString)
        {
            _conn = sConectionString;
        }

        public async Task<bool> PushToQueue<T>(T Message,string sQueue)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting(_conn));

                // Create the queue client.
                CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();

                // Retrieve a reference to a queue.
                CloudQueue queue = queueClient.GetQueueReference(sQueue);

                // Create the queue if it doesn't already exist.
                await queue.CreateIfNotExistsAsync();

                // Create a message and add it to the queue.
                CloudQueueMessage message = new CloudQueueMessage(JsonConvert.SerializeObject(Message));
                await queue.AddMessageAsync(message);
            }
            catch(Exception e)
            {

                return false;
            }
           

            return true;
        }

        
    }
}
