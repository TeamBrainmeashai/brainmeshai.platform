﻿using BrainMesh.Platform.Infrastructure.Interfaces;
using StackExchange.Redis;
using System;
using Newtonsoft;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BrainMesh.Platform.Infrastructure
{
    [Serializable]
    public class RedisCacheManager : ICacheManager,IDisposable
    {
        private Lazy<ConnectionMultiplexer> _lazyConnection;
        private IDatabase _cache;
        private string _connection;


        public RedisCacheManager(string sConnectionString)
        {
            _lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
            {
                string cacheConnection = sConnectionString;
                return ConnectionMultiplexer.Connect(cacheConnection);
            });
            _connection = sConnectionString;
            _cache = _lazyConnection.Value.GetDatabase();
            
        }
                

        public void ClearCache()
        {
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(_connection);
            var server = redis.GetServer(_connection);
            server.FlushDatabase();
        }

        public void Dispose()
        {
            if(_lazyConnection != null)
            {
                _lazyConnection.Value.Close();
                _lazyConnection.Value.Dispose();
            }
        }

        public string ExecuteCommand(string sCommandName, string sParameters)
        {
            return _cache.Execute(sCommandName, sParameters).ToString();
        }

        public bool PingServer()
        {
            return _cache.Execute("PING").ToString().Equals("PONG");
        }

        public object ReadKey(string sKeyName)
        {
            return _cache.StringGet(sKeyName);
        }

        public bool WriteKey(string sKeyName, object oValue, DateTime? dtExpire)
        {
            string json = JsonConvert.SerializeObject(oValue, Formatting.Indented);

            return WriteKey(sKeyName, json, dtExpire);
        }

        public bool WriteKey(string sKeyName, string sValue, DateTime? dtExpire)
        {
            string sReturnValue = _cache.StringGetSet(sKeyName, sValue);

            if(dtExpire != null)
            {
                _cache.KeyExpire(sKeyName, dtExpire);
            }
            
            return sReturnValue != null;
        }
    }
}
